package com.mycompany.protest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 http://www.programmableweb.com/
 http://www.faroo.com/
 http://www.faroo.com/hp/api/api.html
 http://www.faroo.com/api?q=iphone&start=1&length=10&l=en&src=web&f=json
 http://jsonviewer.stack.hu/
 */
import static com.mycompany.protest.Average.call_count;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import edu.uchicago.gerber.yelp.HttpDownUtil;
import edu.uchicago.gerber.yelp.YelpSearchResults;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * FXML Controller class
 *
 * @author ag
 */
public class PdfController implements Initializable {

    private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static String subject = "TESTQUEUE";
    double test_number;
    double previous_price;
    int amount = 10000;
    double average;
    double total;
    //private XYChart.Series series1;
    String GooglePrice;
    String AmazonPrice;
    String FacebookPrice;
    final ToggleGroup RB_Group = new ToggleGroup();
    final ToggleGroup Stock_Group = new ToggleGroup();
    Task task;
    Task task2;
    int pages;//pages in google search
    private StringProperty P1 = new SimpleStringProperty(GooglePrice);
    private StringProperty P2 = new SimpleStringProperty(AmazonPrice);
    private StringProperty P3 = new SimpleStringProperty(FacebookPrice);
    @FXML
    private LineChart<Double, Double> lineChart;
    @FXML
    private RadioButton Google;
    @FXML
    private RadioButton Amazon;
    @FXML
    private RadioButton Facebook;
    @FXML
    private TextField txtSearch;
    @FXML
    private RadioButton Pdf_Radio;
    @FXML
    private RadioButton Doc_Radio;
    @FXML
    private RadioButton Xls_Radio;
    @FXML
    private RadioButton Ppt_Radio;
    @FXML
    private RadioButton Jpg_Radio;
    @FXML
    private TextField txtNumber;
    int number;
    @FXML
    private Button btnGo;
    @FXML
    private Button buttonTrack;
    @FXML
    private TextArea Area;
    //@FXML
    //private ListView<String> lstView;
    @FXML
    private Label lblStatus;
    @FXML
    private Label GPrice;
    @FXML
    private Label FPrice;
    @FXML
    private Label APrice;

    @FXML
    private Label lblTitle;

    YelpSearchResults yelpSearchResultLocal;

    @FXML
    private TableView<DownTask> table;

    private String strDirSave;
    @FXML
    private Button btnSelect;

    @FXML
    private void btnSelect_go(ActionEvent event) {

        DirectoryChooser directoryChooser = new DirectoryChooser();

        directoryChooser.setTitle("This is my file ch");//title of the directory chooser

        //Show open file dialog
        File file = directoryChooser.showDialog(null);

        if (file != null) {

            btnSelect.setText(file.getPath());//show text of the file path
            strDirSave = file.getAbsolutePath();

        }

    }

    //this is not a good implementation because it only searching one page of a google search result. You should either
    //use a webservice like faroo (see TimesMain for example of web-service) or you should search multiple pages (scrape) google for results. 
    //your should be returning approximately 50 documents per search. 
    public class GetReviewsTask extends Task<ObservableList<String>> {

        private String convertSpacesToPluses(String strOrig) {
            return strOrig.trim().replace(" ", "+");
        }

        private String MultiPages(String UrlOrig) {
            pages = pages + 10;
            //String pages = UrlOrig.substring(Math.max(UrlOrig.length() - 2, 0));//last two digits: 10, 20
            String UrlNew = UrlOrig + "&start=" + Integer.toString(pages);
            return UrlNew;
        }

        @Override
        protected ObservableList<String> call() throws Exception {

            ObservableList<String> sales = FXCollections.observableArrayList();
            //updateMessage("Finding pdfs  . . .");
            String strUrl = "https://www.google.com/search?q=";
            String strUrl2 = "http://jpgmag.com/search/photos/";
            strUrl += convertSpacesToPluses(txtSearch.getText());
            strUrl2 += convertSpacesToPluses(txtSearch.getText());
            //strUrl += "+filetype:pdf";
            String filetype = RB_Group.getSelectedToggle().getUserData().toString();
            switch (filetype) {
                case "pdf":
                    strUrl += "+filetype:pdf";//form the string that could be searched in google search
                    break;
                case "doc":
                    strUrl += "+filetype:doc";
                    break;
                case "ppt":
                    strUrl += "+filetype:ppt";
                    break;
                case "xls":
                    strUrl += "+filetype:xls";
                    break;
                case "jpg":
                    filetype = "jpg";
                    break;
                default:
                    strUrl += "+=filetype:pdf";
                    break;
            }
            //strUrl += "+filetype:pdf";//form the string that could be searched in google search
            //this just simulates some work, but since it  and should be in background thread 
            Document doc;

            ArrayList<String> strResults = new ArrayList<>();
            while (strResults.size() < number) {
                //doc = Jsoup.connect(strUrl).userAgent("Mozilla").ignoreHttpErrors(true).timeout(0).get();
                if (!filetype.equals("jpg")) {
                    ///Elements resultLinks = doc.select("h3.r > a");
                    doc = Jsoup.connect(strUrl).userAgent("Mozilla").ignoreHttpErrors(true).timeout(0).get();
                    Elements hrefs = doc.select("a[href]");
                    for (Element href : hrefs) {

                        String strRef = href.attr("abs:href");
                        System.out.println("abs path: " + strRef);
                        if (strRef.contains(filetype) && strRef.contains("https://www.google.com/url?q=") && !strRef.contains("webcache")) {

                            String strPdfToDownload = strRef.substring(strRef.indexOf("https://www.google.com/url?q=") + 29, strRef.indexOf("." + filetype) + 4);
                            strResults.add(strPdfToDownload);

                        }
                        if (strResults.size() > number) {
                            break;
                        }
                    }
                    strUrl = MultiPages(strUrl);
                } else {
                    doc = Jsoup.connect(strUrl2).userAgent("Mozilla").ignoreHttpErrors(true).timeout(0).get();
                    Elements image = doc.select("img");
                    for (Element href : image) {
                        String strRef = href.attr("abs:href");
                        //System.out.println("abs path: " + strRef);
                        String url = href.absUrl("src");
                        System.out.println("The link is " + url);
                        strResults.add(url);
                        if (strResults.size() >= number) {
                            break;
                        }

                    }
                }
                if (strResults == null || strResults.size() == 0) {
                    updateMessage("No data found for that search term...try again");
                } else {
                    updateMessage("pdfs found");
                }
            }

//            NytSearchResults nytResult = new Gson().fromJson(getJSON(strQuery, 0), NytSearchResults.class);
//            ArrayList<String> strResults = nytResult.getUrlValues();
//
            //this will get returned and set to the lstView
            return FXCollections.observableArrayList(strResults);

        }
    }

    private void Suscribe() {
        task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                ConnectionFactory connectionFactory
                        = new ActiveMQConnectionFactory(url);
                //Connection connection = connectionFactory.createConnection();
                final CamelContext context = new DefaultCamelContext();
                context.addComponent("jms",
                        JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
                context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                from("jms:Stock")
                .choice()
                .when(body().contains("Google"))//content-based router
                 .to("jms:topic:TOPIC_Google")
                 .when(body().contains("Amazon"))
                 .to("jms:topic:TOPIC_Amazon")
                 .when(body().contains("Facebook"))
                 .to("jms:topic:TOPIC_Facebook");
                try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

               // from("file:data/outbox?noop=true").to("jms:uchicago");
            }
        });
                context.start();
                Thread.sleep(1000000);
                context.stop();
                return null;
            }
        };
    }
    
    private void Consume() {
        task = new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                ConnectionFactory connectionFactory
                        = new ActiveMQConnectionFactory(url);
                //Connection connection = connectionFactory.createConnection();
                final CamelContext context = new DefaultCamelContext();
                context.addComponent("jms",
                        JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
                context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                from("jms:topic:TOPIC_Google").process(new Processor()
                {
                	public void process(Exchange exchange) throws Exception {
                		String Body = exchange.getIn().getBody(String.class);
                		String[] parts = Body.split(("\\s+"));
                		String StockName = parts[0];
                		String StockPrice = parts[1];
                		//String BidQuan = parts[2];
                		//String AskPrice = parts[3];
                		//String AskQuan = parts[4].substring(0,parts[4].length()-1);
                		StringBuilder csv = new StringBuilder();
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                		csv.append(timeStamp).append("Mountain View");
                		exchange.getIn().setBody(csv.toString());
                		//System.out.println("MESSAGE FROM FILE: " + exchange.getIn().getHeader("CamelFileName") + " is heading to MPCS_51050_LAB7 Queue for Stock :" + body1);
                		}
                }).to("jms:Project");
                from("jms:topic:TOPIC_Amazon").process(new Processor()
                {
                	public void process(Exchange exchange) throws Exception {
                		String Body = exchange.getIn().getBody(String.class);
                		String[] parts = Body.split(("\\s+"));
                		String StockName = parts[0];
                		String StockPrice = parts[1];
                		//String BidQuan = parts[2];
                		//String AskPrice = parts[3];
                		//String AskQuan = parts[4].substring(0,parts[4].length()-1);
                		StringBuilder csv = new StringBuilder();
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                		csv.append(timeStamp).append("Seattle");
                		exchange.getIn().setBody(csv.toString());
                		//System.out.println("MESSAGE FROM FILE: " + exchange.getIn().getHeader("CamelFileName") + " is heading to MPCS_51050_LAB7 Queue for Stock :" + body1);
                		}
                }).to("jms:Project");
                from("jms:topic:TOPIC_Facebook").process(new Processor()
                {
                	public void process(Exchange exchange) throws Exception {
                		String Body = exchange.getIn().getBody(String.class);
                		String[] parts = Body.split(("\\s+"));
                		String StockName = parts[0];
                		String StockPrice = parts[1];
                		//String BidQuan = parts[2];
                		//String AskPrice = parts[3];
                		//String AskQuan = parts[4].substring(0,parts[4].length()-1);
                		StringBuilder csv = new StringBuilder();
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                		csv.append(timeStamp).append("Silicon Valley");
                		exchange.getIn().setBody(csv.toString());
                		//System.out.println("MESSAGE FROM FILE: " + exchange.getIn().getHeader("CamelFileName") + " is heading to MPCS_51050_LAB7 Queue for Stock :" + body1);
                		}
                }).to("jms:Project");
            }
        });
                
                context.start();
                Thread.sleep(1000000);
                context.stop();
                return null;
            }
        };
    }
    
    

    private void initRsvp() throws InterruptedException {
        Suscribe();
        new Thread(task).start();
        Thread.sleep(1000);
        Consume();
        new Thread(task).start();
        //Thread.sleep(500);
        //Consume();
       // new Thread(task).start();
        //final XYChart.Series<Double, Double> series1 = new XYChart.Series<>();
        //final ObservableList<XYChart.Series<Double, Double>> lineChartData = FXCollections.observableArrayList();
        //final LineChart.Series<Double, Double> series1 = new LineChart.Series<Double, Double>();
        task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final XYChart.Series<Double, Double> series1 = new XYChart.Series<>();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        lineChart.getData().addAll(series1);
                    }
                });

                //lineChart.getData().addAll(series1);
                Document doc;
                buttonTrack.setDisable(true);
                String stockUrl = "";
                String stock = Stock_Group.getSelectedToggle().getUserData().toString();
                //String stock = convertSpacesToPluses(txtSearch.getText());
                switch (stock) {
                    case "Google":
                        stockUrl = "https://www.google.com/finance?q=google&ei=rKz6U5iSIZLfqAHGm4CwAQ";
                        break;
                    case "Amazon":
                        stockUrl = "https://www.google.com/finance?q=amazon&ei=tqz6U-C8C5TdqAHT-ICgDw";
                        break;
                    case "Facebook":
                        stockUrl = "https://www.google.com/finance?q=facebook&ei=FOL6U9DEGIKuqAHcsIGQAQ";
                        break;
                    default:
                        stockUrl = "";
                        break;
                }

                try {

                    while (true) {
                        doc = Jsoup.connect(stockUrl).userAgent("Mozilla").ignoreHttpErrors(true).timeout(0).get();
                        Elements elements = doc.select("meta");
                        for (Element e : elements) {
                            if (e.attr("itemprop").equals("price")) {
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                                System.out.println(timeStamp + ": " + e.attr("content"));
                                switch (stock) {
                                    case "Google":
                                        GooglePrice = e.attr("content");
                                        Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    GPrice.setText(GooglePrice);
                                                    double p1 = Double.parseDouble(GooglePrice);

                                                    if (previous_price < 0.1) {
                                                    } else if (p1 < previous_price) {
                                                        amount += (int) (previous_price - p1) * 10000;
                                                        System.out.printf("Decision: BUY %d Google Stock\n", (int) (p1 - previous_price) * 10000);
                                                        System.out.printf("Still left %d Google Stock", amount);
                                                    } else {
                                                        amount += (int) (previous_price - p1) * 10000;
                                                        System.out.printf("Decision: Sell %d Google Stock\n", (int) (p1 - previous_price) * 10000);
                                                        System.out.printf("Still left %d Google Stock\n", amount);
                                                    }
                                                    Average x = tensec_average.getInstance();
                                                    average = x.average(average, p1);
                                                    System.out.println(x.stock_count);
                                                    System.out.println(average);
                                                    series1.getData().add(new XYChart.Data<Double, Double>(test_number, Double.parseDouble(GooglePrice)));
                                                    previous_price = p1;
                                                    /**
                                                     * ********************************************
                                                     */
                                                    ConnectionFactory connectionFactory
                                                            = new ActiveMQConnectionFactory(url);
                                                    //Connection connection = connectionFactory.createConnection();
                                                    final CamelContext context = new DefaultCamelContext();
                                                    context.addComponent("jms",
                                                            JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
                                                    context.start();
                                                    ProducerTemplate template = context.createProducerTemplate();
                                                    template.sendBody("jms:Google", "Google: " + GooglePrice);
                                                    template.sendBody("jms:Stock", "Google: "+ GooglePrice);
                                                    context.stop();
                                                    //connection.start();
                                                    // Session session = connection.createSession(false,
                                                    //       Session.AUTO_ACKNOWLEDGE);
                                                    // Destination destination = session.createQueue(subject);
                                                    // MessageProducer producer = session.createProducer(destination);
                                                    // TextMessage message = session.createTextMessage(GooglePrice);
                                                    // producer.send(message);
                                                    // System.out.println("Sent message '" + message.getText() + "'");

                                                    // connection.close();
                                                } catch (JMSException ex) {
                                                    Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                                } catch (Exception ex) {
                                                    Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                                }

                                            }
                                        });
                                        break;
                                    case "Amazon":
                                        AmazonPrice = e.attr("content");
                                        Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                {
                                                    try {
                                                        APrice.setText(AmazonPrice);
                                                        double p1 = Double.parseDouble(AmazonPrice);

                                                        if (previous_price < 0.1) {
                                                        } else if (p1 < previous_price) {
                                                            amount += (int) (previous_price - p1) * 10000;
                                                            System.out.printf("Decision: BUY %d Amazon Stock\n", (int) (p1 - previous_price) * 10000);
                                                            System.out.printf("Still left %d Amazon Stock", amount);
                                                        } else {
                                                            amount += (int) (previous_price - p1) * 10000;
                                                            System.out.printf("Decision: Sell %d Amazon Stock\n", (int) (p1 - previous_price) * 10000);
                                                            System.out.printf("Still left %d Amazon Stock\n", amount);
                                                        }
                                                        Average x = tensec_average.getInstance();
                                                        average = x.average(average, p1);
                                                        System.out.println(x.stock_count);
                                                        System.out.println(average);
                                                        series1.getData().add(new XYChart.Data<Double, Double>(test_number, Double.parseDouble(AmazonPrice)));
                                                        previous_price = p1;
                                                        /**
                                                         * ********************************************
                                                         */
                                                        ConnectionFactory connectionFactory
                                                                = new ActiveMQConnectionFactory(url);
                                                        //Connection connection = connectionFactory.createConnection();
                                                        final CamelContext context = new DefaultCamelContext();
                                                        context.addComponent("jms",
                                                                JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
                                                        context.start();
                                                        ProducerTemplate template = context.createProducerTemplate();
                                                        template.sendBody("jms:Amazon", "Amazon: " + AmazonPrice);
                                                        template.sendBody("jms:Stock", "Amazon: "+ AmazonPrice);
                                                        context.stop();
                                                    //connection.start();
                                                        // Session session = connection.createSession(false,
                                                        //       Session.AUTO_ACKNOWLEDGE);
                                                        // Destination destination = session.createQueue(subject);
                                                        // MessageProducer producer = session.createProducer(destination);
                                                        // TextMessage message = session.createTextMessage(GooglePrice);
                                                        // producer.send(message);
                                                        // System.out.println("Sent message '" + message.getText() + "'");

                                                        // connection.close();
                                                    } catch (JMSException ex) {
                                                        Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                                    } catch (Exception ex) {
                                                        Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                                    }

                                                }

                                            }
                                        });

                                        break;
                                    case "Facebook":
                                        FacebookPrice = e.attr("content");
                                        Platform.runLater(new Runnable() {
                                            @Override
                                            public void run() {
                                                {
                                                    try {
                                                        FPrice.setText(FacebookPrice);
                                                        double p1 = Double.parseDouble(FacebookPrice);

                                                        if (previous_price < 0.1) {
                                                        } else if (p1 < previous_price) {
                                                            amount += (int) (previous_price - p1) * 10000;
                                                            System.out.printf("Decision: BUY %d Facebook Stock\n", (int) (p1 - previous_price) * 10000);
                                                            System.out.printf("Still left %d Facebook Stock", amount);
                                                        } else {
                                                            amount += (int) (previous_price - p1) * 10000;
                                                            System.out.printf("Decision: Sell %d Facebook Stock\n", (int) (p1 - previous_price) * 10000);
                                                            System.out.printf("Still left %d Facebook Stock\n", amount);
                                                        }
                                                        Average x = tensec_average.getInstance();
                                                        average = x.average(average, p1);
                                                        System.out.println(x.stock_count);
                                                        System.out.println(average);
                                                        series1.getData().add(new XYChart.Data<Double, Double>(test_number, Double.parseDouble(FacebookPrice)));
                                                        previous_price = p1;
                                                        /**
                                                         * ********************************************
                                                         */
                                                        ConnectionFactory connectionFactory
                                                                = new ActiveMQConnectionFactory(url);
                                                        //Connection connection = connectionFactory.createConnection();
                                                        final CamelContext context = new DefaultCamelContext();
                                                        context.addComponent("jms",
                                                                JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
                                                        context.start();
                                                        ProducerTemplate template = context.createProducerTemplate();
                                                        template.sendBody("jms:Facebook", "Facebook: " + FacebookPrice);
                                                        template.sendBody("jms:Stock", "Facebook: " + FacebookPrice);
                                                        context.stop();
                                                    //connection.start();
                                                        // Session session = connection.createSession(false,
                                                        //       Session.AUTO_ACKNOWLEDGE);
                                                        // Destination destination = session.createQueue(subject);
                                                        // MessageProducer producer = session.createProducer(destination);
                                                        // TextMessage message = session.createTextMessage(GooglePrice);
                                                        // producer.send(message);
                                                        // System.out.println("Sent message '" + message.getText() + "'");

                                                        // connection.close();
                                                    } catch (JMSException ex) {
                                                        Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                                    } catch (Exception ex) {
                                                        Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                                    }

                                                }
                                            }
                                        });
                                        break;
                                    default:
                                        break;
                                }

                                // series1.getData().add(new XYChart.Data(test_number, Double.valueOf(e.attr("content"))));
                                break;                              //XYChart
                            }
                            //lineChart.getData().addAll(series1);  
                            test_number++;
                        }
                        Thread.sleep(1000);
                        //test_number++ ;
                    }

                } catch (IOException ex) {
                    Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;

            }

        };
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //series1.getData().add(new XYChart.Data<Double, Double>(test_number, 1.0));
        //series1 = new XYChart.Series<Number, Number>();
        //lineChart.getData().addAll(series1);
        //GPrice.textProperty().bind(P1);
        Pdf_Radio.setUserData("pdf");
        Doc_Radio.setUserData("doc");
        Xls_Radio.setUserData("xls");
        Ppt_Radio.setUserData("ppt");
        Jpg_Radio.setUserData("jpg");
        Google.setUserData("Google");
        Amazon.setUserData("Amazon");
        Facebook.setUserData("Facebook");
        Pdf_Radio.setToggleGroup(RB_Group);
        Doc_Radio.setToggleGroup(RB_Group);
        Xls_Radio.setToggleGroup(RB_Group);
        Ppt_Radio.setToggleGroup(RB_Group);
        Jpg_Radio.setToggleGroup(RB_Group);
        Google.setToggleGroup(Stock_Group);
        Amazon.setToggleGroup(Stock_Group);
        Facebook.setToggleGroup(Stock_Group);
        strDirSave = null;

        buttonTrack.setOnAction(
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event
                    ) {
                        try {
                            initRsvp();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        new Thread(task).start();

                    }
                }
        );

        //System.out.println("hello world");
        // table = new TableView<DownTask>();
        TableColumn<PdfController.DownTask, String> statusCol = new TableColumn("Status");

        statusCol.setCellValueFactory(
                new PropertyValueFactory<PdfController.DownTask, String>(
                        "message"));
        statusCol.setPrefWidth(
                100);

        TableColumn<PdfController.DownTask, Double> progressCol = new TableColumn("Progress");

        progressCol.setCellValueFactory(
                new PropertyValueFactory<PdfController.DownTask, Double>(
                        "progress"));

        progressCol.setPrefWidth(
                125);

        //this is the most important call
        progressCol.setCellFactory(ProgressBarTableCell.<PdfController.DownTask>forTableColumn());

        TableColumn<PdfController.DownTask, String> fileCol = new TableColumn("File");

        fileCol.setCellValueFactory(
                new PropertyValueFactory<PdfController.DownTask, String>(
                        "title"));
        fileCol.setPrefWidth(
                375);

        //add the cols
        table.getColumns()
                .addAll(statusCol, progressCol, fileCol);
        btnGo.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent
                    ) {
                        String number_in = txtNumber.getText();
                        if (txtNumber.getText().equals("")) {
                            number = 10;//detect whether input textfield is empty
                        } else {
                            number = Integer.parseInt(number_in);
                        }
                        if (strDirSave == null) {
                            System.err.println("You need to set an output dir!");
                            System.out.println(number);//10 is default number
                            //System.out.println(RB_Group.getSelectedToggle().getUserData().toString());
                            return;
                        }
                        /*if (txtNumber.getText() == null)
                         {
                         System.err.println("You need to input a number");
                         return;
                         }*/
                        //you can instantiate a new Task each time if you want to "re-use" it, but once this intance is cancelled, you can't restart it again. 
                        final Task<ObservableList<String>> getReviewsTask = new GetReviewsTask();

                        table.getItems().clear();
                        lblStatus.textProperty().bind(getReviewsTask.messageProperty());
                        btnGo.disableProperty().bind(getReviewsTask.runningProperty());
                        //lstView.itemsProperty().bind(getReviewsTask.valueProperty());

                        getReviewsTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                            @Override
                            public void handle(WorkerStateEvent t) {

                                ObservableList<String> observableList = getReviewsTask.getValue();
                                if (observableList == null || observableList.size() == 0) {
                                    return;
                                }

                                //add Tasks to the table
                                for (String str : getReviewsTask.getValue()) {
                                    table.getItems().add(new DownTask(str, strDirSave));
                                }

                                //fire up executor-service with limted number of threads
                                ExecutorService executor = Executors.newFixedThreadPool(3, new ThreadFactory() {
                                    @Override
                                    public Thread newThread(Runnable r) {
                                        Thread t = new Thread(r);
                                        t.setDaemon(true);
                                        return t;
                                    }
                                });

                                for (PdfController.DownTask pbarTask : table.getItems()) {
                                    executor.execute(pbarTask);
                                }

                            }
                        });

                        new Thread(getReviewsTask).start();
                        new Thread(new Runnable() {

                            @Override
                            public void run() {

                                try {
                                    Thread.sleep(1000);

                                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(PdfController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                        }).start();

                    }
                }
        );

    }

    class DownTask extends Task<Void> {

        private String pdfFrom, pdfTo;

        public DownTask(String pdfFrom, String pdfTo) {
            this.pdfFrom = pdfFrom;
            this.pdfTo = pdfTo;

        }

        @Override
        protected Void call() throws Exception {

            HttpDownUtil util = new HttpDownUtil();
            try {

                this.updateProgress(ProgressIndicator.INDETERMINATE_PROGRESS, 1);
                this.updateMessage("Waiting...");
                util.downloadFile(this.pdfFrom);

                InputStream inputStream = util.getInputStream();
                // opens an output stream to save into file
                FileOutputStream outputStream = new FileOutputStream(pdfTo + "/" + util.getFileName());
                this.updateTitle(pdfTo + "/" + util.getFileName());

                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                long totalBytesRead = 0;
                int percentCompleted = 0;
                long fileSize = util.getContentLength();

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                    totalBytesRead += bytesRead;
                    percentCompleted = (int) (totalBytesRead * 100 / fileSize);
                    updateProgress(percentCompleted, 100);
                }

                updateMessage("done");
                outputStream.close();
                util.disconnect();

            } catch (Exception ex) {
                ex.printStackTrace();
                this.cancel(true);
                table.getItems().remove(this);

            }

            return null;
        }

    }

//this is a UI-thread-blocking operation and should be called in background thread
    public String getJSON(String url, int timeout) {
        try {
            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            // Logger.getLogger(DebugServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}

abstract class Average {

    //abstract double MD(bond x);
    abstract double average(double x, double y);
    public static int stock_count;
    public static int call_count;
    public double total;//current total price
    public double now; //current price
    public double result;

}

class sec_average extends Average {

    private static sec_average instance = null;

    public static sec_average getInstance() {
        if (instance == null) {
            instance = new sec_average();
            System.out.println("create a new Average instance");
        } else {
            System.out.println("Singleton pattern");
        }
        return instance;
    }

    @Override
    public double average(double x, double y) {
        this.now = y;
        //this.total += this.total+this.now;
        this.total = x * call_count;
        result = (this.total + this.now) / (++call_count);
        return result;
    }
}

class tensec_average extends Average {

    private static tensec_average instance = null;
    private static int call_count = 10;

    public static tensec_average getInstance() {
        if (instance == null) {
            instance = new tensec_average();
            System.out.println("create a new Average instance");
        } else {
            System.out.println("Singleton pattern");
        }
        return instance;
    }

    @Override
    public double average(double x, double y) {
        //call_count ++;
        if (call_count < 10) {
            call_count++;
            return result;
        } else {
            call_count = 0;
            //stock_count ++;
            this.now = y;
            //this.total += this.total+this.now;
            this.total = x * stock_count;
            result = (this.total + this.now) / (++stock_count);
            return result;
        }

    }
}
